var saveButton, saveOtherButton, list;

/**
 * Render list
 */
var currentAccount = function () {
  // Get current uid and image from DOM
  var name = dropdown.querySelector('.DashUserDropdown-userInfo .fullname');
  var username = dropdown.querySelector('.DashUserDropdown-userInfo .username');
  var img = document.querySelector('.dropdown-toggle img');
  if (!img) return false;
  
  var src = img.getAttribute('src').replace('_normal','');
  return {
      uid: img.getAttribute('data-user-id'),
      name: name.textContent,
      username: username.textContent,
      img: src
  };
};
var generateCard = function (account){
	return 	'<div class="card-content">'
			+		'<nav class="level is-mobile">'
			+			'<!-- Left side -->'
			+			'<a href="https://twitter.com/'+account.username+'" target="_blank">'
			+			'<div class="level-left">'
			+				'<div class="level-item">'
			+				'	<figure class="image is-64x64">'
			+						'<img src="'+account.img+'">'
			+					'</figure>'
			+				'</div>'
			+				'<div class="level-item">'
			+					'<div class="content">	<strong>'+account.name+'</strong> '
			+						'<small>'+account.username+'<small>'
			+					'</div>'
			+				'</div>'
			+		'</div>'
			+			'</a>'			
			+		'<div class="level-right">'
			+		'<div class="level-item">'
			+			'	<div class="field is-grouped">'
			+					'<p class="control">'
			+	  					'<a class="button is-info is-outlined" id="checkbox">'
			+							'<span class="icon is-small">'
			+								'<i class="far fa-check-square"></i>'
			+							'</span>'
			+						'</a>'
			+	 				 '</p>'
			+					'<p class="control is-hidden-mobile">'
			+						'<a class="button is-danger is-outlined" id="delete">'
			+							'<span class="icon is-small">'
			+							'	<i class="fas fa-times"></i>'
			+							'</span>'
			+						'</a>'
			+					'</p>'
			+				'</div>'
			+			'</div>'
			+			'</div>'
			+		'</nav>'
			+	'</div>';
	
}
var render = function (data) {
  // Accounts
  var accounts = data.accounts;
  var parent = document.getElementById('accounts');

	  if (Object.keys(accounts).length == 0){
		   var message = document.createElement('article');
		   message.classList.add('message');
		   message.classList.add('is-info');
		   message.innerHTML = '<div class="message-body">'
							 + 	 'You currently have no accounts logged into Twitter'
							 +   '<br/>'
							 +   '<a class="button is-info" href="https://twitter.com/login" rel="noopener noreferrer" target="_blank">'
						     +   	'<span>'
							 +			'<i class="fab fa-twitter"></i> '
							 +		'<span>'
							 +		'<span>Sign Into Twitter</span>'
							 +   '</a>'
							 + '</div>';
	parent.appendChild(message);
	  } else {
		  
  var cookie = browser.cookies.get({
        url: 'https://twitter.com',
        name: 'auth_token'
      },  function(value){
		  if (!value){
		  var message = document.createElement('article');
		  message.classList.add('message');
		  message.classList.add('is-warning');
		  message.innerHTML = '<div class="message-header">'
							+   '<p>Warning</p>'
							+ '</div>'
							+ '<div class="message-body">'
							+	'You have been logged out of Twitter, please log in to enable account switching'
							+ '</div>'
		  parent.insertAdjacentElement('afterbegin',message);
		  }
		  
		  Object.keys(accounts).forEach(function (uid) {
			var account = accounts[uid];
			var card = document.createElement('div');
			card.classList.add('card');
			card.innerHTML = generateCard(account);
			card = parent.appendChild(card);
			var cbox = card.getElementsByClassName('is-info')[0];
			if (account.token === value.value){
				cbox.classList.remove('is-info');
				cbox.classList.add('is-success');
			}
			cbox.addEventListener('click', function () {
				var success = parent.getElementsByClassName('is-success');
				Array.prototype.forEach.call(success, function(boxes) {
					boxes.classList.remove('is-success');
					boxes.classList.add('is-info');
				});
				 cbox.classList.remove('is-info');
				 cbox.classList.add('is-success');
				 setCurrent(uid);
			});
			var dbox = card.getElementsByClassName('is-danger')[0];
			dbox.addEventListener('click', function () {
			  remove(uid, card);
			});
			
		  });
	  });
		}
};

/**
 * Save state
 */

var setCurrent = function (uid) {
  browser.runtime.sendMessage({ type: 'switchAccount', uid: uid }, function() {});
};

/**
 * Delete Account
 */

var remove = function (uid, card) {
  browser.runtime.sendMessage({ type: 'removeAccount', uid: uid }, function() {
    card.parentNode.removeChild(card);
  });
};


/**
 * Init
 */

document.addEventListener('DOMContentLoaded', function () {
	browser.runtime.sendMessage({ type: 'getAccounts' }, render);

});
