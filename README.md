# Twitcher For Firefox - Twitter Account Switcher

Firefox add-on that allows the user to have multiple accounts connected to their twitter with easy switching

## Usage

For the Firefox version of this add-on, you can find it here: https://addons.mozilla.org/en-US/firefox/addon/twitcher-for-firefox/

For the Google Chrome version of this add-on, you can find it here: https://chrome.google.com/webstore/detail/twitcher-twitter-account/gmngpagflejjoblmmamaonmnkghjmebh

## License
[GNU GENERAL PUBLIC LICENSE, Version 3]https://gitlab.com/MattShinkfield/twitcher-for-firefox/raw/ad72b36b81f6d5fa8c473d0850c709693f14b7ed/LICENSE

Code styling mainly provided by the CSS framework Bulma, developed by Jeremy Thomas. 
	Available at: https://bulma.io/
Icons mainly provided by the toolkit Font Awesome, developed by Fonticons, Inc
	Available at: https://fontawesome.com/

## NOTICE

Twitcher was originally developed by Thom Seddon, he worked on basically all of this and all credit should go to him.

I am a Mozilla Firefox fan of his Google Chrome extension and ported it over with a bit of cleaning up and api changes.
