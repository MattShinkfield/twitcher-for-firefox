// Globals
var COOKIE_TOKEN = 'auth_token';
var COOKIE_ID = 'twid';

/**
 * Convenience methods for accessing accounts
 */

var accounts = {
  accounts: {},
  get: function (uid) {
    return this.accounts[uid];
  },
  set: function (uid, data, callback) {
    this.accounts[uid] = data;
    if (callback) this.save(callback);
  },
  remove: function (uid, callback) {
    delete this.accounts[uid];
    if (callback) this.save(callback);
  },
  save: function (callback) {
    var accounts = this.accounts;
    var json = JSON.stringify(accounts);
    browser.storage.sync.set({ 'accounts': json }, function () {
      callback(accounts);
    });
  },
  getAll: function (callback) {
    if (!callback) return this.accounts;
    var self = this;
    browser.storage.sync.get('accounts', function (json) {
      if (json.accounts && json.accounts.length) {
        self.accounts = JSON.parse(json.accounts);
      }
      callback(self.accounts);
    });
  },
  setAll: function (accounts, callback) {
    this.accounts = accounts;
    if (callback) this.save(callback);
  }
};

/**
 * Message handlers
 */

var handlers = {

  getAccounts: function (request, sendResponse) {
    accounts.getAll(function (accounts) {

      browser.storage.sync.get('settings', function (data) {
        sendResponse({
          accounts: accounts
        });
      });
    });

    return true;
  },

  removeAccount: function (request, sendResponse) {
    accounts.remove(request.uid, sendResponse);
    return true;
  },
  removeAllAccounts: function (request) {
    console.log("test2");
    return true;
  },

  switchAccount: function (request) {
    var account = accounts.get(request.uid);
    var expires = new Date;
    expires.setFullYear(expires.getFullYear() + 10);

    if (account) {
      // Set token cookie
      browser.cookies.set({
        url: 'https://twitter.com',
        name: COOKIE_TOKEN,
        value: account.token,
        domain: '.twitter.com',
        path: '/',
        secure: true,
        httpOnly: true,
        expirationDate: expires / 1000
      });

      // Remove user id cookie
      browser.cookies.remove({
        url: 'https://twitter.com',
        name: COOKIE_ID
      });
    }

    // Reload
    browser.tabs.query({url:'https://twitter.com/*'}, function (tabs) {
		tabs.forEach(function(tab){
			browser.tabs.reload(tab.id);
		});
    });
  },

  logout: function (request) {
    // Delete cookie
    browser.cookies.remove({
      url: 'https://twitter.com',
      name: COOKIE_TOKEN
    });

    // Redirect
    browser.tabs.query({url:'https://twitter.com/*'}, function (tabs) {
		tabs.forEach(function(tab){
		  browser.tabs.update(tab.id, {
			url: 'https://twitter.com/login'
		  });
	  });
    });	
  },

  // Get current account
  currentAccount: function (request, sendResponse) {

    var getToken = function (next) {
      browser.cookies.get({
        url: 'https://twitter.com',
        name: COOKIE_TOKEN
      }, setCookie);
    };

    var setCookie = function (cookie) {
      if (!cookie) return sendResponse();

      // Save cookie
      var account = request.currentAccount;
      accounts.set(account.uid, {
        name: account.name,
        username: account.username,
        img: account.img,
        token: cookie.value
      }, sendResponse);
    };

    // Make sure we have go all accounts before saving
    accounts.getAll(getToken);

    return true;
  },

  // Update other settings
  settings: function (request, sendResponse) {
    browser.storage.sync.get('settings', function (data) {
      var settings = data.settings || { 'show-donate': true };

      for (var key in request.settings) {
        settings[key] = request.settings[key];
      }

      browser.storage.sync.set({ 'settings': settings }, function () {
        sendResponse();
      });
    });

    return true;
  }
};

/**
 * Listen for messages
 */

browser.runtime.onMessage.addListener(function (request, sender, sendResponse) {
	
  if (sender.id !== "{be547f71-51f1-4d8f-bca2-0f25a222cc65}")
   return;

  if (request.type in handlers)
    return handlers[request.type](request, sendResponse);

  return false;
});
 